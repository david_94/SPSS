# SPSS - Social Perception and Sentiment in Soccer
>Stima e previsione di prestazioni sportive di atleti e squadre

## Operating System Compatibility

* Android 6+
* iOS 10+
* Windows 7/8/8.1/10
* MacOS X
* Linux [every distro with compatible browser]

## Browser Compatibility

* Google Chrome 63+
* Mozilla Firefox 56+

## Usage

* See README.md in dev-workspace folder

## Release History

* 0.1.1 [2018-06-18]
 * Changed dev-workspace for new code
 * Updated all documentation
* 0.1.0 [2018-04-17]
 * Added dev-workspace
 * Added all documents [1st revision] 
* 0.0.1 [2017-11-14]
 * First commit

## Meta
| Autore          | Matricola |
|-----------------|-----------|
| Davide Merletti | 781122    |
| Giacomo Imola   | 783431    |
| Nicolas Perillo | 780921    |
| Paolo Velati    | 781621    |
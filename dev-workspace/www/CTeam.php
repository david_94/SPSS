<?php
	class CTeam {
		private $m_id;
		private $m_teamName;
		private $m_coachName;
		private $m_stadium;
		private $m_players;
		private $m_roles;
		private $m_shieldImg;
		private $m_datapoints;
		private $m_smiles;
		private $m_current_season = '2017-18';
		private $m_pg;
    	private $m_pt;
    	private $m_v;
	    private $m_p;
	    private $m_s;
	    private $m_gf;
	    private $m_gs;
	    private $m_dr;
	    private $m_amm;
	    private $m_esp;
		private $m_conn;


		//Costruttore
		function __construct($conn, $teamName){
			$this->m_teamName = $teamName;
			$this->m_conn = $conn;
	    }


		//Getters
	    public function getName() {
	    	return $this->m_teamName;
	    }
	    public function getCoach() {
	    	return $this->m_coachName;
	    }
	    public function getStadium() {
	    	return $this->m_stadium;
	    }
	    public function getPlayers() {
	    	return $this->m_players;
	    }
	    public function getRoles() {
	    	return $this->m_roles;
	    }
	    public function getImg() {
	    	return $this->m_shieldImg;
	    }
	    public function getDataPoints() {
	    	return $this->m_datapoints;
	    }
	    public function getSmiles() {
	    	return $this->m_smiles;
	    }
	    public function getPG () {
        	return $this->m_pg;
      	}
      	public function getPT () {
        	return $this->m_pt;
      	}
	    public function getV () {
	       return $this->m_v;
	      }
	    public function getP () {
	        return $this->m_p;
	    }
	    public function getS () {
	        return $this->m_s;
	    }
	    public function getGF () {
	        return $this->m_gf;
	    }
	    public function getGS () {
	        return $this->m_gs;
	    }
	    public function getDR () {
	        return $this->m_dr;
	    }
	    public function getAMM () {
	        return $this->m_amm;
	    }
	    public function getESP () {
	    	return $this->m_esp;
	    }


	    //Estrai dati squadra
		public function extractData() {
    		$result = mysqli_query($this->m_conn, "SELECT * from teams WHERE Nome = '$this->m_teamName'");
    		$count = 0;
		    if (mysqli_num_rows($result) > 0) {
		      	while($row = mysqli_fetch_assoc($result)) {
			        //$this->m_id = $row["idTeam"];
			        $this->m_coachName = $row["Coach"];
			        $this->m_stadium = $row["stadium"];
			        $this->m_shieldImg = $row["shieldImg"];
			        $this->m_players = [];
			        $this->m_roles = [];
			        $count++;
      			}
    		}
    		return $count;
   		}
		// ---end


	    //Estrai giocatori da team
		public function getPlayersByTeamID() {
			$resultplayers = mysqli_query($this->m_conn, "SELECT DISTINCT * from giocatori WHERE Squadra = '$this->m_teamName' AND Stagione = '$this->m_current_season' GROUP BY Nome ORDER BY Ruolo DESC");	
			$num = 0;
			if (mysqli_num_rows($resultplayers) > 0) {	
				while ($row = $resultplayers->fetch_assoc()) {
	        		$this->m_players[$num] = $row["Nome"];
	        		$this->m_roles[$num] = $row["Ruolo"];
	        		$num++;
	      		}
	      		return $num;
	      	}
   		}
		// ---end


		//Funzione per integrare un bar chart a partire dai dati dell'array dataPoints
		public function generateChart() {	      	
			$tot = 100;
	      	$frand = rand(0,100);
	      	$srand = 100-$frand;
	      	$this->m_datapoints = array(
	          	array("y" => $frand,"label" => "Altro" ),
	          	array("y" => $srand,"label" => "Fantagazzetta" )
	      	);
		}
		// ---end


		//Funzione per la generazione dei 3 smile, di cui uno colorato
		public function generateSmiles($selectedOne) {
			$this->m_smiles = array(
              0 => "data/happy", 
              1 => "data/neut", 
              2 => "data/nhappy"
            );            
            for ($i=0; $i<3; $i++){
              if ($i==$selectedOne){
                $this->m_smiles[$i] = $this->m_smiles[$i] . "_selected.png";
              }
              else{
                $this->m_smiles[$i] = $this->m_smiles[$i] . ".png";
              }
            }
		}
		// ---end


		//Generazione valori casuali
		public function generateRand() {
	        $this->m_pg = rand(1,50);
	        $this->m_pt = rand(1,50);
	        $this->m_v = rand(1,50);
	        $this->m_p = rand(1,50);
	        $this->m_s = rand(1,50);
	        $this->m_gf = rand(1,50);
	        $this->m_gs = rand(1,50);
	        $this->m_dr = rand(1,50);
	        $this->m_amm = rand(1,50);
	        $this->m_esp = rand(1,50);
    	}
	}  
?>
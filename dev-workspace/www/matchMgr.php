<?php
  include 'db.php';
  require_once 'bootstrap.php';
  include 'CMatch.php';
  $conn = OpenCon();

  $teamfirst = $_POST["team1"];
  $teamsecond = $_POST["team2"];

  //Instantiate new player
  $thismatch = new CMatch($conn, $teamfirst, $teamsecond);

  //Check if player exist and extact data
  $match_exist = $thismatch->extractData();
  $matchExistTrue = $match_exist[0]>0;
  $arrayofmatches = $match_exist[1];
  $sizeofmatches = sizeof($arrayofmatches);
  if ($matchExistTrue) {
    $thismatch->getTeamsImgs();
    $thismatch->calculateCurrPron();
    $thismatch->generateChart();
    $thismatch->generatePronostic();

    echo $twig->render('matchpage.html', array('matchExist'=>$matchExistTrue, 'thismatch'=>$thismatch, 'numofmatches'=>$sizeofmatches, 'arraymatches'=>$arrayofmatches));
  }
  else{
    echo $twig->render('matchpage.html', array('matchExist'=>$matchExistTrue));
  }
  
  CloseCon($conn);
?>
<?php
	class CPlayer {
		private $m_id;
		private $m_name;
		private $m_fullName;
		private $m_number;
		private $m_role;
		private $m_img;
		private $m_team;
		private $m_teamImg;
		private $m_datapoints;
		private $m_smiles;
		private $m_stagione;
		private $m_conn;


		//Constructor
		function __construct($conn, $playerFullName, $stagione){
			$this->m_fullName = $playerFullName;
			$this->m_stagione = $stagione;
			$this->m_conn = $conn;
	    }


	    //Getters
	    public function getFullName() {
	    	return $this->m_fullName;
	    }
	    public function getNumber() {
	    	return $this->m_number;
	    }
	    public function getRole() {
	    	return $this->m_role;
	    }
	    public function getImg() {
	    	return $this->m_img;
	    }
	    public function getTeam() {
	    	return $this->m_team;
	    }
	    public function getTeamImg() {
	    	return $this->m_teamImg;
	    }
	    public function getDataPoints() {
	    	return $this->m_datapoints;
	    }
	    public function getSmiles() {
	    	return $this->m_smiles;
	    }


	   	public function getVoto() {
	    	return $this->m_voto;
	    }
	    public function getFantaVoto() {
	    	return $this->m_fantavoto;
	    }
	    public function getGol() {
	    	return $this->m_gol;
	    }
	    public function getAssist() {
	    	return $this->m_assist;
	    }
	    public function getRigori() {
	    	return $this->m_rigori;
	    }
	    public function getRigoriSbagliati() {
	    	return $this->m_rigoriSbagliati;
	    }
	    public function getAutogol() {
	    	return $this->m_autoGol;
	    }
	    public function getAmmonizioni() {
	    	return $this->m_ammonizioni;
	    }
	    public function getEspulsioni() {
	    	return $this->m_espulsioni;
	    }	    
	    public function getStagione() {
	    	return $this->m_stagione;
	    }


	    //Check if season exist
	    public function seas() {
    		$result = mysqli_query($this->m_conn, "SELECT * from giocatori WHERE Nome = '$this->m_fullName' and Stagione = '$this->m_stagione'");
    		$count = 0;
		    if (mysqli_num_rows($result) > 0) {
		      	while($row = mysqli_fetch_assoc($result)) {
			        $this->m_id = $row["id"];
			        $count++;
      			}
    		}
    		return $count;
   		}


	    //Player's data
		public function extractData() {
    		$result = mysqli_query($this->m_conn, "SELECT * from giocatori WHERE Nome = '$this->m_fullName' and Stagione = '$this->m_stagione'");
    		$count = 0;
		    if (mysqli_num_rows($result) > 0) {
		      	while($row = mysqli_fetch_assoc($result)) {
			        $this->m_id = $row["id"];
			        $this->m_role = $row["Ruolo"];
			        $this->m_number = $row["NumeroMaglia"];
			        $this->m_team = $row["Squadra"];
			        $this->m_voto = $row["Voto"];
			        $this->m_fantavoto = $row["FantaVoto"];
			        $this->m_gol = $row["Gol"];
			        $this->m_assist = $row["Assist"];
			        $this->m_rigori = $row["Rigori"];
			        $this->m_rigoriSbagliati = $row["RigoriSbagliati"];
			        $this->m_autoGol = $row["Autogol"];	        
			        $this->m_ammonizioni = $row["Ammonizioni"];        
			        $this->m_espulsioni = $row["Espulsioni"];       
			        $this->m_espulsioni = $row["Espulsioni"];      
			        $this->m_stagione = $row["Stagione"];
					$this->m_img = "data/profilepic.jpg";
			        $count++;
      			}
    		}
    		return $count;
   		}
		// ---end


	    //Player's team
		public function getTeamByPlayerID() {
       		$listofimages = mysqli_query($this->m_conn, "SELECT shieldImg from teams WHERE Nome = '$this->m_team'");
      		while ($row = $listofimages->fetch_assoc()) {
       			$this->m_teamImg = $row["shieldImg"];
      		}
   		}
		// ---end


		//Funzione per integrare un bar chart a partire dai dati dell'array dataPoints
		public function generateChart() {
	      	$tot = 100;
	      	$frand = rand(0,100);
	      	$srand = 100-$frand;
			//dato id player, si estrae il relativo array di dati
	      	$this->m_datapoints = array(
	          	array("y" => $frand,"label" => "Altro" ),
	          	array("y" => $srand,"label" => "Fantagazzetta" )
	      	);
		}
		// ---end


		//Funzione per la generazione dei 3 smile, di cui uno colorato
		public function generateSmiles($selectedOne) {
			$this->m_smiles = array(
              0 => "data/happy", 
              1 => "data/neut", 
              2 => "data/nhappy"
            );            
            for ($i=0; $i<3; $i++){
              if ($i==$selectedOne){
                $this->m_smiles[$i] = $this->m_smiles[$i] . "_selected.png";
              }
              else{
                $this->m_smiles[$i] = $this->m_smiles[$i] . ".png";
              }
            }
		}
		// ---end
	}  
?>
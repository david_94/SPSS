<?php
  include 'db.php';
  require_once 'bootstrap.php';
  include 'CTeam.php';
  $conn = OpenCon();

  $teamname = $_POST["team"];

  //Instantiate new player
  $thisteam = new CTeam($conn, $teamname);

  //Check if player exist and extact data
  $teamExist = $thisteam->extractData()>0;
  if ($teamExist) {
    $numbOfPlayers = $thisteam->getPlayersByTeamID();
    $listOfSurnames = $thisteam->getPlayers();
    $listOfRoles = $thisteam->getRoles();
    $thisteam->generateChart();    
    $activeSmileNumb = rand (0, 2);
    $thisteam->generateRand();
    echo $twig->render('teampage.html', array('teamExist'=>$teamExist, 'thisteam'=>$thisteam, 'numbOfPlayers'=>$numbOfPlayers, 'listOfSurnames'=>$listOfSurnames, 'listOfRoles'=>$listOfRoles));
  }
  else{
    echo $twig->render('teampage.html', array('teamExist'=>$teamExist));
  }
  
  CloseCon($conn);
?>
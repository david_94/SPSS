<?php
	class CMatch {
		private $m_id;
		private $m_teamfirst;
		private $m_teamsecond;
		private $m_time;
		private $m_result;
		private $m_teamfirstID;
		private $m_teamsecondID;
		private $m_thefirstImg;
		private $m_thesecondImg;
		private $m_dataPoints;
		private $m_currMatchPron;
		private $m_pron;
		private $m_conn;


		//Getters
		public function getTeamF() {
			return $this->m_teamfirst;
		}
		public function getTeamS() {
			return $this->m_teamsecond;			
		}		
		public function getTeamFImg() {
			return $this->m_thefirstImg;
		}
		public function getTeamSImg() {
			return $this->m_thesecondImg;			
		}
		public function getPronostic() {
			return $this->m_pron;			
		}
		public function getDataPoints() {
			return $this->m_dataPoints;			
		}
		public function getTime() {
			return $this->m_time;			
		}		
		public function getResult() {
			return $this->m_result;			
		}		
		public function getCurrMatchPron() {
			return $this->m_currMatchPron;			
		}

		//Costruttore
		function __construct($conn, $teamfirst, $teamsecond){
			$this->m_teamfirst = $teamfirst;
			$this->m_teamsecond = $teamsecond;
			$this->m_conn = $conn;
	    }


	    //Estrai dati match
		public function extractData() {
			/*$resultteam1 = mysqli_query($this->m_conn, "SELECT idTeam from teams WHERE Nome = '$this->m_teamfirst'");
			while ($row = $resultteam1->fetch_assoc()) {
        		$this->m_teamfirstID = $row["idTeam"];
      		}			
      		$resultteam2 = mysqli_query($this->m_conn, "SELECT idTeam from teams WHERE Nome = '$this->m_teamsecond'");
			while ($row = $resultteam2->fetch_assoc()) {
        		$this->m_teamsecondID = $row["idTeam"];
      		}*/

    		$result = mysqli_query($this->m_conn, "SELECT * from matches WHERE (teamfirst = '$this->m_teamfirst' AND teamsecond = '$this->m_teamsecond')");
    		$result2 = mysqli_query($this->m_conn, "SELECT * from matches WHERE (teamfirst = '$this->m_teamsecond' AND teamsecond = '$this->m_teamfirst')");
    		
    		$count = 0;
    		$list=array();    		
    		$matchesdata=array();

		    if (mysqli_num_rows($result) > 0) {
		      	while($row = mysqli_fetch_assoc($result)) {
			        $this->m_id = $row["idMatch"];
			        $this->m_time = $row["time"];
			        $this->m_result = $row["Risultato"];
			        $current = array($this->m_teamfirst, $this->m_teamsecond, $this->m_time, $this->m_result, $this->calculateCurrPron());
			        array_push($matchesdata,$current);
			        $count++;
      			}
    		}		    
    		if (mysqli_num_rows($result2) > 0) {
		      	while($row = mysqli_fetch_assoc($result2)) {
			        $this->m_id = $row["idMatch"];
			        $this->m_time = $row["time"];
			        $this->m_result = $row["Risultato"];
			        $current = array($this->m_teamsecond, $this->m_teamfirst, $this->m_time, $this->m_result, $this->calculateCurrPron());
			        array_push($matchesdata,$current);
			        $count++;
      			}
    		}
    		array_unshift($list, $matchesdata);
    		array_unshift($list, $count);
    		return $list;
   		}
		// ---end


	    //Estrai dati match
		public function calculateCurrPron() {
			$resultString = explode("-", $this->m_result);
			$firstNumber = $resultString[0];
			$secondNumber = $resultString[1];
			if ((int)$firstNumber > (int)$secondNumber)
				return 1;
			if ((int)$firstNumber < (int)$secondNumber)
				return 2;
			if ((int)$firstNumber == (int)$secondNumber)
				return "X";
   		}
		// ---end


	    //Estrai immagini teams
		public function getTeamsImgs() {
      		$firstteamImg = mysqli_query($this->m_conn, "SELECT shieldImg from teams WHERE Nome = '$this->m_teamfirst'");		    
      		if (mysqli_num_rows($firstteamImg) > 0) {
		      	while($row = mysqli_fetch_assoc($firstteamImg)) {
			        $this->m_thefirstImg = $row["shieldImg"];
      			}
    		}      		
      		$secondteamImg = mysqli_query($this->m_conn, "SELECT shieldImg from teams WHERE Nome = '$this->m_teamsecond'");
      		if (mysqli_num_rows($secondteamImg) > 0) {
		      	while($row = mysqli_fetch_assoc($secondteamImg)) {
			        $this->m_thesecondImg = $row["shieldImg"];
      			}
      		}

      		if (empty($this->m_thefirstImg))
				$firstteamImg = "The selected image isn't described in our database, please update the table 'teams'";			
			if (empty($this->m_thesecondImg))
				$secondteamImg = "The selected image isn't described in our database, please update the table 'teams'";
   		}
		// ---end


		//Funzione per integrare un bar chart a partire dai dati dell'array dataPoints
		public function generateChart() {	      	
			$tot = 100;
	      	$frand = rand(0,100);
	      	$srand = rand(0,100-$frand);
	      	$trand = 100-$srand-$frand;
			//dato id player, si estrae il relativo array di dati
	      	$this->m_dataPoints = array(
	          	array("y" => $frand,"label" => "Pareggi" ),
	          	array("y" => $srand,"label" => $this->m_teamsecond ),	          	
	          	array("y" => $trand,"label" => $this->m_teamfirst )
	      	);
		}
		// ---end


		//Funzione per integrare un bar chart a partire dai dati dell'array dataPoints
		public function generatePronostic() {
				$this->m_pron = array(
	              0 => "data/pron1", 
	              1 => "data/pronX", 
	              2 => "data/pron2"
	            );            
	            $selected = rand(0,2);
	            for ($i=0; $i<3; $i++){
	              if ($i==$selected){
	                $this->m_pron[$i] = $this->m_pron[$i] . "_selected.png";
	              }
	              else{
	                $this->m_pron[$i] = $this->m_pron[$i] . ".png";
	              }
	            }
		}
		// ---end
	}  
?>
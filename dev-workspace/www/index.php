  <?php
    include 'db.php';
    require_once 'bootstrap.php';
    $conn = OpenCon();
    
    //List of player names and surnames
    $result = mysqli_query($conn, "SELECT DISTINCT Nome from giocatori");
    if (mysqli_num_rows($result) > 0) {
      while($row = mysqli_fetch_assoc($result)) {
          $stringName = $row["Nome"];
          $stringSeparated = explode(" ", $stringName);
          $surnames[] = $stringSeparated[0];
      }
    }  

    //List of team names  
    $resultteams = mysqli_query($conn, "SELECT * from teams");
    if (mysqli_num_rows($resultteams) > 0) {
      while($row = mysqli_fetch_assoc($resultteams)) {
          $namesteams[] = $row["Nome"];
      }
    }

    //Render page sending arrays
    echo $twig->render('indexpage.html', array('surnames' => $surnames, 'namesteams' => $namesteams));

    CloseCon($conn);
  ?>
<?php
  include 'db.php';
  require_once 'bootstrap.php';
  include 'CPlayer.php';
  $conn = OpenCon();

  $playername = $_POST["player"];
  $season = $_POST["season"];

  //Instantiate new player
  $thisplayer = new CPlayer($conn, $playername, $season);
  print($thisplayer->seas()<0);
 
  //If current season doesn't exist, return the more recent
  if ($thisplayer->seas()<1)
    $thisplayer = new CPlayer($conn, $playername, "2017-18");  
      if ($thisplayer->seas()<1)
        $thisplayer = new CPlayer($conn, $playername, "2016-17"); 
          if ($thisplayer->seas()<1)
            $thisplayer = new CPlayer($conn, $playername, "2015-16");
              if ($thisplayer->seas()<1)
                $thisplayer = new CPlayer($conn, "Empty", "2015-16");
    

  //Check if player exist and extact data
  $playerExist = $thisplayer->extractData()>0;
  if ($playerExist) {
    $thisplayer->getTeamByPlayerID();
    $thisplayer->generateChart();          
    $activeSmileNumb = rand (0, 2);
    $thisplayer->generateSmiles($activeSmileNumb);
    echo $twig->render('playerpage.html', array('playerExist' => $playerExist, 'thisplayer' => $thisplayer, 'season' => $season ));
  }
  else{
    echo $twig->render('playerpage.html', array('playerExist' => $playerExist));
  }
  
  CloseCon($conn);
?>
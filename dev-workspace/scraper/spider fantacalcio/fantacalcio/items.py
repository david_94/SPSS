# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class FantacalcioItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass

# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field

class PlayerItem(Item):

    PlayerName = Field()
    Rating = Field()
    PotentialRating = Field()

    Height = Field()
    Weight = Field()
    PreferredFoot = Field()
    BirthDate = Field()
    Age = Field()
    PreferredPositions = Field()
    PlayerWorkRate = Field()
    BallControl = Field()
    Dribbling = Field()
    Marking = Field()
    SlideTackle = Field()
    StandTackle = Field()
    Aggression = Field()
    Reactions = Field()
    AttPosition = Field()
    Interceptions = Field()
    Vision = Field()
    Composure = Field()
    Crossing = Field()
    ShortPass = Field()
    LongPass = Field()
    Acceleration = Field()
    Stamina = Field()
    Strength = Field()
    Balance = Field()
    SprintSpeed = Field()
    Agility = Field()
    Jumping = Field()
    Heading = Field()
    ShotPower = Field()
    Finishing = Field()
    LongShots = Field()
    Curve = Field()
    FKAcc = Field()
    Penalties = Field()
    Volleys = Field()
    GKPositioning = Field()
    GKDiving = Field()
    GKHandling = Field()
    GKKicking = Field()
    GKReflexes = Field()

    PlayerId = Field()
    SeasonId = Field()

    #tmp link per past seasons
    TmpLink = Field()
    PlayerPageLink = Field()

    #attributo per le vecchie versioni
    Anticipation = Field()
    Creativity = Field()

    Nationality = Field()

    NationalTeam = Field()
    KitNumberNationalTeam = Field()
    ClubTeam = Field()
    KitNumberClubTeam = Field()
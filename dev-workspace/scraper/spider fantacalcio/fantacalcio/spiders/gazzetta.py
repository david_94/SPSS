# -*- coding: utf-8 -*-
import scrapy
from scrapy.item import Item, Field
from scrapy.selector import Selector
import string

class GazzettaSpider(scrapy.Spider):
    name = 'gazzetta'
    custom_settings = {
        'ITEM_PIPELINES': {
            'fantacalcio.pipelines.GazzettaPipeline': 100
        }
    }
    allowed_domains = ['gazzetta.it']
    start_urls = ['https://www.gazzetta.it/calcio/fantanews/voti/serie-a-2017-18/',
        'https://www.gazzetta.it/calcio/fantanews/voti/serie-a-2016-17/',
        'https://www.gazzetta.it/calcio/fantanews/voti/serie-a-2015-16/']

    def parse(self, response):

        selector = Selector(response)  # in alternativa response.selector.xpath('//title/text()')
        listaGiornate = selector.xpath('//nav[contains(@class, "secondary-menu")]/ul[contains(@class,"menuDaily")]/li/a/@href').extract()
        for giornata in listaGiornate:
            request = scrapy.Request('https:' + giornata, callback=self.parseGiornata)
            request.meta['stagione'] = response.request.url[-8:-1]
            yield request


    def parseGiornata(self, response):
        selector = Selector(response)
        stagione = response.meta['stagione']
        # playerSel = selector.xpath('//a[contains(@href,"player")]/@href')#.extract()
        listaSquadre = selector.xpath('//div[contains(@class, "singleRound")]/div/ul[contains(@class, "magicTeamList")]/li[contains(@class, "head")]/div[contains(@class, "teamName")]/span[contains(@class, "teamNameIn")]/text()').extract()[0:20]
        print(listaSquadre)
        # boxSquadre = selector.xpath('//div[contains(@class, "MXXX-central-articles-main-column")]/div[contains(@class, "magicDayList listView magicDayListChkDay")]/iv[contains(@class, "singleRound")]')
        preBoxSquadre = selector.xpath('//a[contains(@data-anchor, "#")]/@data-anchor').extract() #ne trova giustamente venti
        boxSquadre1 = selector.xpath('//a[contains(@data-anchor, "#")]/following-sibling::div[1]').extract()
        boxSquadre = selector.xpath('//a[contains(@data-anchor, "#")]/following-sibling::div[1]')
        # h1[contains(text(), 'Text 1')]/following-sibling::div[1]/text()
        #print(boxSquadre.__len__())
        print(type(boxSquadre1[0]))
        # print(type(boxSquadre2))
        giornata = selector.xpath('//section/div/div[contains(@class, "magicDayList")]/h4/text()').extract()[0]
        giornata = giornata[:giornata.index('^')]
        print(giornata)
        i = 0
        for team in preBoxSquadre:
            basicXpath = '//a[contains(@data-anchor, "' + team + '")]/following-sibling::div[1]'
            listaNome = selector.xpath(basicXpath + '/div/ul/li/div/div/span/a/text()').extract()
            listaNumeroMaglia = selector.xpath(basicXpath + '/div/ul/li/div/div/span[contains(@class, "playerNumber")]/text()').extract()
            listaRuolo = selector.xpath(basicXpath + '/div/ul/li/div/div/span[contains(@class, "playerRole")]/text()').extract()
            #print(listaRuolo) #sballata, corretto con un *2
            listaVoto = selector.xpath(basicXpath + '/div/ul/li/div[contains(@class, "playerName")]/following-sibling::div[1]//text()').extract()
            listaGol = selector.xpath(basicXpath + '/div/ul/li/div[contains(@class, "playerName")]/following-sibling::div[2]//text()').extract()
            listaAssist = selector.xpath(basicXpath + '/div/ul/li/div[contains(@class, "playerName")]/following-sibling::div[3]//text()').extract()
            listaRigori = selector.xpath(basicXpath + '/div/ul/li/div[contains(@class, "playerName")]/following-sibling::div[4]//text()').extract()
            listaRigoriSbagliati = selector.xpath(basicXpath + '/div/ul/li/div[contains(@class, "playerName")]/following-sibling::div[5]//text()').extract()
            listaAutogol = selector.xpath(basicXpath + '/div/ul/li/div[contains(@class, "playerName")]/following-sibling::div[6]//text()').extract()
            listaAmmonizioni = selector.xpath(basicXpath + '/div/ul/li/div[contains(@class, "playerName")]/following-sibling::div[7]//text()').extract()
            listaEspulsioni = selector.xpath(basicXpath + '/div/ul/li/div[contains(@class, "playerName")]/following-sibling::div[8]//text()').extract()
            listaFantaVoto = selector.xpath(basicXpath + '/div/ul/li/div[contains(@class, "playerName")]/following-sibling::div[9]//text()').extract()
            j = 0
            for nome in listaNome:
                item = Giocatore()
                item['Stagione'] = stagione
                item['Squadra'] = preBoxSquadre[i][1:].title()
                item['Nome'] = nome
                item['NumeroMaglia'] = int(listaNumeroMaglia[j])
                item['Voto'] = cleanStatsVoto(listaVoto[j])
                item['Ruolo'] = listaRuolo[j*2]
                item['Gol'] = cleanStats(listaGol[j])
                item['Rigori'] = cleanStats(listaRigori[j])
                item['RigoriSbagliati'] = cleanStats(listaRigoriSbagliati[j])
                item['Autogol'] = cleanStats(listaAutogol[j])
                item['Ammonizioni'] = cleanStats(listaAmmonizioni[j])
                item['Espulsioni'] = cleanStats(listaEspulsioni[j])
                item['FantaVoto'] = cleanStatsVoto(listaFantaVoto[j])
                item['Giornata'] = int(giornata)
                j += 1
                yield item
            print(team)
            i += 1
            #print(tmpPlayer)
            # item['PlayerPageLink'] = pastSeasonsList[i]
            # if i == 0:
            #     item = self.getId(item, response)
            #     item = self.getBodyPanel(item, response)
            #     item = self.getTeamAndName(item, response)
            #     yield item
            # else:
            #     request = scrapy.Request(season, callback=self.parsePlayerPastSeasons)
            #     request.meta['Item'] = item
            #     yield request

        #print(boxSquadre[0])
        #print(preBoxSquadre)
        # print(boxSquadre.__len__())
        #print(preBoxSquadre)
        # '//div[contains(@class,"panel-body")]'
        # response.xpath("//a/img/@src")

        # linkGiornata = selector.xpath('//a[contains(@href,"player")]/@href').re(r"\/player\/\d{1,6}.*")
        # playerSel = list(set(playerSel))
        pass

def cleanStats(input):
    tmpCleanStat = input.replace('\n','').replace('\t','').replace('\r','').replace(' ','')
    if tmpCleanStat == '-':
        cleanStat = 0
    else:
        cleanStat = int(tmpCleanStat)
    return cleanStat

def cleanStatsVoto(input):
    tmpCleanStat = input.replace('\n','').replace('\t','').replace('\r','').replace(' ','')
    if tmpCleanStat == '-':
        cleanStat = 0
    else:
        cleanStat = float(tmpCleanStat)
    return cleanStat

class Giocatore(Item):

    Squadra = Field()
    Giornata = Field()
    Nome = Field()
    NomeCompleto = Field()
    NumeroMaglia = Field()
    Ruolo = Field()
    Voto = Field()
    FantaVoto = Field()
    Gol = Field()
    Assist = Field()
    Rigori = Field()
    RigoriSbagliati = Field()
    Autogol = Field()
    Ammonizioni = Field()
    Espulsioni = Field()
    Stagione = Field()

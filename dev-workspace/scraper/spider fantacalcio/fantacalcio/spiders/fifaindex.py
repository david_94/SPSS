# -*- coding: utf-8 -*-
import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.selector import Selector
from twisted.internet import reactor
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from scrapy.item import Item, Field
#import os, sys
#parentPath = os.path.abspath("..")
#if parentPath not in sys.path:
#    sys.path.insert(0, parentPath)
#from items import PlayerItem

# try:
#     import sys
#     sys.path.append('..')
#     from items import PlayerItem #unresolved reference ma non e' un problema di codice
# except ImportError:
#     print("non mi prende items ;(")
#     pass




class FifaindexSpider(scrapy.Spider):
    name = 'fifaindex'
    custom_settings = {
        'ITEM_PIPELINES': {
            'fantacalcio.pipelines.FifaindexPipeline' : 100
        }
    }
    allowed_domains = ['fifaindex.com']
    # start_urls = ['http://fifaindex.com/']
    start_urls = ['https://www.fifaindex.com/team/45/juventus/',
                  'https://www.fifaindex.com/team/48/napoli/'
                  'https://www.fifaindex.com/team/52/roma/',
                  'https://www.fifaindex.com/team/44/inter/',
                  'https://www.fifaindex.com/team/46/lazio/',
                  'https://www.fifaindex.com/team/47/milan/',
                  'https://www.fifaindex.com/team/54/torino/',
                  'https://www.fifaindex.com/team/110374/fiorentina/',
                  'https://www.fifaindex.com/team/39/atalanta/',
                  'https://www.fifaindex.com/team/189/bologna/',
                  'https://www.fifaindex.com/team/192/chievo-verona/',
                  'https://www.fifaindex.com/team/110556/genoa/',
                  'https://www.fifaindex.com/team/1837/sampdoria/',
                  'https://www.fifaindex.com/team/111974/sassuolo/',
                  'https://www.fifaindex.com/team/55/udinese/',
                  'https://www.fifaindex.com/team/112791/spal/',
                  'https://www.fifaindex.com/team/1842/cagliari/',
                  'https://www.fifaindex.com/team/206/hellas-verona/',
                  'https://www.fifaindex.com/team/112026/benevento/',
                  'https://www.fifaindex.com/team/110734/crotone/',
                  'https://www.fifaindex.com/team/45/juventus/fifa17_173/',
                  'https://www.fifaindex.com/team/48/napoli/fifa17_173/',
                  'https://www.fifaindex.com/team/52/roma/fifa17_173/',
                  'https://www.fifaindex.com/team/44/inter/fifa17_173/',
                  'https://www.fifaindex.com/team/46/lazio/fifa17_173/',
                  'https://www.fifaindex.com/team/47/milan/fifa17_173/',
                  'https://www.fifaindex.com/team/54/torino/fifa17_173/',
                  'https://www.fifaindex.com/team/110374/fiorentina/fifa17_173/',
                  'https://www.fifaindex.com/team/39/atalanta/fifa17_173/',
                  'https://www.fifaindex.com/team/189/bologna/fifa17_173/',
                  'https://www.fifaindex.com/team/192/chievo-verona/fifa17_173/',
                  'https://www.fifaindex.com/team/110556/genoa/fifa17_173/',
                  'https://www.fifaindex.com/team/1837/sampdoria/fifa17_173/',
                  'https://www.fifaindex.com/team/111974/sassuolo/fifa17_173/',
                  'https://www.fifaindex.com/team/55/udinese/fifa17_173/',
                  'https://www.fifaindex.com/team/1843/palermo/fifa17_173/',
                  'https://www.fifaindex.com/team/1842/cagliari/fifa17_173/',
                  'https://www.fifaindex.com/team/200/pescara/fifa17_173/',
                  'https://www.fifaindex.com/team/1746/empoli/fifa17_173/',
                  'https://www.fifaindex.com/team/110734/crotone/fifa17_173/',
                  'https://www.fifaindex.com/team/45/juventus/fifa16_73/',
                  'https://www.fifaindex.com/team/48/napoli/fifa16_73/',
                  'https://www.fifaindex.com/team/52/roma/fifa16_73/',
                  'https://www.fifaindex.com/team/44/inter/fifa16_73/',
                  'https://www.fifaindex.com/team/46/lazio/fifa16_73/',
                  'https://www.fifaindex.com/team/47/milan/fifa16_73/',
                  'https://www.fifaindex.com/team/54/torino/fifa16_73/',
                  'https://www.fifaindex.com/team/110374/fiorentina/fifa16_73/',
                  'https://www.fifaindex.com/team/39/atalanta/fifa16_73/',
                  'https://www.fifaindex.com/team/189/bologna/fifa16_73/',
                  'https://www.fifaindex.com/team/192/chievo-verona/fifa16_73/',
                  'https://www.fifaindex.com/team/110556/genoa/fifa16_73/',
                  'https://www.fifaindex.com/team/1837/sampdoria/fifa16_73/',
                  'https://www.fifaindex.com/team/111974/sassuolo/fifa16_73/',
                  'https://www.fifaindex.com/team/55/udinese/fifa16_73/',
                  'https://www.fifaindex.com/team/111657/frosinone/fifa16_73/',
                  'https://www.fifaindex.com/team/1842/cagliari/fifa16_73/',
                  'https://www.fifaindex.com/team/206/hellas-verona/fifa16_73/',
                  'https://www.fifaindex.com/team/112409/carpi/fifa16_73/',
                  'https://www.fifaindex.com/team/110734/crotone/fifa16_73/']


    def parse(self, response):
        n = 100
        selector = Selector(response) #in alternativa response.selector.xpath('//title/text()')
        res = selector.xpath('//title/text()').extract() #con css: response.css('title::text')
        #playerSel = selector.xpath('//a[contains(@href,"player")]/@href')#.extract()
        playerSel = selector.xpath('//a[contains(@href,"player")]/@href').re(r"\/player\/\d{1,6}.*")
        playerSel = list(set(playerSel))
        if False:
            print(properSpace('response:',30), response)
            print(properSpace('type(response):',30), type(response))
            print(properSpace('selector:',30), selector)
            print(properSpace('type(selector):', 30), type(selector))
            print(properSpace('selector.xpath:',30),selector.xpath('//a').extract())
            print(properSpace('type(selector.xpath()):', 30), type(res))
            print(properSpace('playerSel:',30), playerSel)
            print(properSpace('type(playerSel):',30), type(playerSel)) #scrapy.selector.unified.SelectorList
            print(properSpace('playerSel:',30), playerSel)
            print(properSpace('type(playerSel):',30),type(playerSel)) #list
            print(properSpace('res:',30),res)
            print(properSpace('type(playerSel[0]):',30),type(playerSel[0]))
            print(properSpace('playerSel.__len__():',30),playerSel.__len__())
        playerList = []
        for playerSel1 in playerSel[0:n]:
            playerSel1 = 'https://www.fifaindex.com' + playerSel1
            playerId = list(map(str, playerSel1.split('/')))[4]
            print(playerSel1)
            playerList.append(playerSel1)
            request = scrapy.Request(playerSel1, callback=self.parsePlayer)
            request.meta['PlayerId'] = playerId
            request.meta['Link'] = playerSel1
            yield request

    def parsePlayer(self,response):
        selector = Selector(response)
        currentEdition = 18
        firstEditionRecorded = 7
        seasonCount = currentEdition - firstEditionRecorded
        tmpPastSeasonsList = selector.xpath('//a[contains(@href,"player")]/@href').re(r"\/player\/\d{1,6}.*\/fifa.*")
        pastSeasonsList = []
        pastSeasonsList.append(response.meta['Link'])
        i = 0
        for season in tmpPastSeasonsList[0:seasonCount]:
            pastSeasonsList.append('https://www.fifaindex.com' + season)
        i = 0
        for season in pastSeasonsList: #ma ora anche quella attuale
            item = PlayerItem()
            item['PlayerId'] = response.meta['PlayerId']
            item['PlayerPageLink'] = pastSeasonsList[i]
            if i == 0:
                item = self.getId(item, response)
                item = self.getBodyPanel(item, response)
                item = self.getTeamAndName(item, response)
                yield item
            else:
                request = scrapy.Request(season, callback=self.parsePlayerPastSeasons)
                request.meta['Item'] = item
                yield request
            i += 1
            #return item

    def parsePlayerPastSeasons(self,response):
        #print('Response', response)
        item = response.meta['Item']
        #item['PlayerId'] = response.meta['PlayerId']
        item = self.getId(item, response)
        item = self.getBodyPanel(item, response)
        item = self.getTeamAndName(item, response)
        return item


    def getBodyPanel(self, item, response):
        selector = Selector(response)
        statList = selector.xpath('//div[contains(@class,"panel-body")]//text()[normalize-space()]').extract() #normalize-space() mi serve per eliminare i newline
        statList = [x.strip(' ') for x in statList]
        tmp = []
        i = 0
        for obj in statList:
            obj = obj.replace(' ', '')
            obj = obj.replace('.', '')
            if i == 1:
                obj = obj.replace('cm', '')
            if i == 3:
                obj = obj.replace('kg', '')
            tmp.append(obj)
            i += 1
        if 'PlayerWorkRate' in tmp: #le vecchie edizioni non hanno PlayerWorkRate
            preferredPositions = tmp[(tmp.index('PreferredPositions') + 1):tmp.index('PlayerWorkRate')]
            del tmp[(tmp.index('PreferredPositions')+1):tmp.index('PlayerWorkRate')]
        elif 'WeakFoot' in tmp:
            preferredPositions = tmp[(tmp.index('PreferredPositions') + 1):tmp.index('WeakFoot')]
            del tmp[(tmp.index('PreferredPositions') + 1):tmp.index('WeakFoot')]
        #se dovessi tornare sulle stagioni piu' vecchie rivedere questo
        #else:
        #    preferredPositions = tmp[(tmp.index('PreferredPositions') + 1):]
        preferredPositions = ",".join(preferredPositions) #lo elaboro qui e non nella pipeline
        tmp.insert((tmp.index('PreferredPositions')+1), preferredPositions)
        if 'WeakFoot' in tmp:
            del tmp[tmp.index('WeakFoot'):tmp.index('BallControl')]
        else:
            del tmp[(tmp.index('PreferredPositions')+2):tmp.index('BallControl')]
        attrIndex = 0
        for attr in tmp:
            if attrIndex % 2 == 0:
                #attributi differenti per le vecchie versioni
                if attr == 'Tackling':
                    item['SlideTackle'] = tmp[attrIndex + 1]
                    item['StandTackle'] = tmp[attrIndex + 1]
                elif attr == 'Handling':
                    item['GKHandling'] = tmp[attrIndex + 1]
                elif attr == 'Reflexes':
                    item['GKReflexes'] = tmp[attrIndex + 1]
                elif attr == 'Passing':
                    item['ShortPass'] = tmp[attrIndex + 1]
                elif attr == 'LongBalls':
                    item['LongPass'] = tmp[attrIndex + 1]
                elif attr == 'ShotAccuracy':
                    item['FKAcc'] = tmp[attrIndex + 1]
                elif attr == 'Pace' or attr == 'Rushing':
                    pass
                else:
                    item[attr] = tmp[attrIndex + 1]
            attrIndex += 1
        return item

    def getTeamAndName(self, item, response):
        selector = Selector(response)
        item['Nationality'] = selector.xpath('//a[contains(@href,"nationality")]/@title').extract()[0]
        statList = selector.xpath('//div[contains(@class,"panel-body")]//text()[normalize-space()]').extract()  # normalize-space() mi serve per eliminare i newline
        statList = [x.strip(' ') for x in statList]
        tmp = []
        i = 0
        for obj in statList:
            obj = obj.replace(' ', '')
            obj = obj.replace('.', '')
            if i == 1:
                obj = obj.replace('cm', '')
            if i == 3:
                obj = obj.replace('kg', '')
            tmp.append(obj)
            i += 1
        #print(tmp)
        tmp2 = []
        for element in tmp:
            tmp2.append(element)
        panelTitle = selector.xpath('//h3[contains(@class,"panel-title")]//text()').extract()
        playerName = panelTitle[0]
        #print(panelTitle)
        item['PlayerName'] = playerName[:-1]
        item['Rating'] = panelTitle[1]
        item['PotentialRating'] = panelTitle[3]
        if 'KitNumber' in tmp and tmp.count('KitNumber') >= 1:
            kitNumberIndex = [tmp2.index('KitNumber')]
            del tmp2[kitNumberIndex[0]]
            teamList = [panelTitle[5]]
            if tmp.count('KitNumber') == 2:  # c'e' anche il nome della nazionale ed uno spazio vuoto per il simbolo della stessa
                kitNumberIndex.append(tmp2.index('KitNumber'))
                teamList.append(panelTitle[7])
                #print(tmp[kitNumberIndex[0] + 1], tmp[kitNumberIndex[1] + 2])
            if len(teamList) == 2:
                if tmp[tmp.index('KitNumber') + 2] == 'JoinedClub' or tmp[tmp.index('KitNumber') + 2] == 'ContractLength':
                    item['ClubTeam'] = teamList[0]
                    item['KitNumberClubTeam'] = tmp[kitNumberIndex[0] + 1]
                    item['NationalTeam'] = teamList[1]
                    item['KitNumberNationalTeam'] = tmp[kitNumberIndex[1] + 2]
                else:
                    item['NationalTeam'] = teamList[0]
                    item['KitNumberNationalTeam'] = tmp[kitNumberIndex[0] + 1]
                    item['ClubTeam'] = teamList[1]
                    item['KitNumberClubTeam'] = tmp[kitNumberIndex[1] + 2]
            elif len(teamList) == 1:
                if tmp[tmp.index('KitNumber') + 2] == 'JoinedClub' or tmp[tmp.index('KitNumber') + 2] == 'ContractLength':
                    item['ClubTeam'] = teamList[0]
                    item['KitNumberClubTeam'] = tmp[kitNumberIndex[0] + 1]
                else:
                    item['NationalTeam'] = teamList[0]
                    item['KitNumberNationalTeam'] = tmp[kitNumberIndex[0] + 1]
        return item

    def getPastSeasons(self, response):
        item = response.meta['Item']
        pastSeasonsList = response.meta['SeasonsList']
        for season in pastSeasonsList:
            request = scrapy.Request(season, callback=self.parsePlayerPastSeasons)
            request.meta['Item'] = item
            request.meta['PlayerId'] = item['PlayerId']
            yield request

    def getId(self, item, response):
        selector = Selector(response)
        titleContainingSeasonId = selector.xpath('//meta[contains(@property,"og:title")]').re(r"FIFA.*")
        item['SeasonId'] = list(map(str, titleContainingSeasonId[0].split()))[1]
        return item

def properSpace(input, margin):
    length = len(input)
    #print('len:', length)
    for _ in range(1, margin - length - 1):
        input = input + '-'
    input = input + '>'
    return input


class PlayerItem(Item):

    Acceleration = Field()
    Age = Field()
    Aggression = Field()
    Agility = Field()
    Anticipation = Field()  # attributo per le vecchie versioni
    AttPosition = Field()
    Balance = Field()
    BallControl = Field()
    BirthDate = Field()
    ClubTeam = Field()
    Composure = Field()
    Creativity = Field()  # attributo per le vecchie versioni
    Crossing = Field()
    Curve = Field()
    Dribbling = Field()
    FKAcc = Field()
    Finishing = Field()
    GKDiving = Field()
    GKHandling = Field()
    GKKicking = Field()
    GKPositioning = Field()
    GKReflexes = Field()
    Heading = Field()
    Height = Field()
    Interceptions = Field()
    Jumping = Field()
    KitNumberClubTeam = Field()
    KitNumberNationalTeam = Field()
    LongPass = Field()
    LongShots = Field()
    Marking = Field()
    NationalTeam = Field()
    Nationality = Field()
    Penalties = Field()
    PlayerId = Field()
    PlayerName = Field()
    PlayerPageLink = Field()
    PlayerWorkRate = Field()
    PotentialRating = Field()
    PreferredFoot = Field()
    PreferredPositions = Field()
    Rating = Field()
    Reactions = Field()
    SeasonId = Field()
    ShortPass = Field()
    ShotPower = Field()
    SlideTackle = Field()
    SprintSpeed = Field()
    Stamina = Field()
    StandTackle = Field()
    Strength = Field()
    Vision = Field()
    Volleys = Field()
    Weight = Field()

# process = CrawlerProcess({
#     'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
# })
#
# process.crawl(FifaindexSpider)
# process.start() # the script will block here until the crawling is finished

#
# configure_logging({'LOG_FORMAT': '%(levelname)s: %(message)s'})
# runner = CrawlerRunner()
#
# d = runner.crawl(FifaindexSpider)
# d.addBoth(lambda _: reactor.stop())
# reactor.run() # the script will block here until the crawling is finished
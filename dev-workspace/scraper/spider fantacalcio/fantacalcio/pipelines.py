# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


# class FantacalcioPipeline(object):
#     def process_item(self, item, spider):
#         return item


# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import sqlite3 as lite

con = None

class GazzettaPipeline(object):
    def __init__(self):
        self.setupDBConn()
        self.createTables()

    def setupDBConn(self):
        self.con = lite.connect('C:/Users/Giacomo/fantacalcio/test.db')
        self.cur = self.con.cursor()

    def createTables(self):
        self.dropGiocatoriTable()
        self.createGiocatoriTable()

    def dropGiocatoriTable(self):
        self.cur.execute("DROP TABLE IF EXISTS Giocatori")

    def createGiocatoriTable(self):
        self.cur.execute("CREATE TABLE IF NOT EXISTS Giocatori(id INTEGER PRIMARY KEY NOT NULL, \
            Squadra TEXT, \
            Giornata INT, \
            Nome TEXT, \
            NomeCompleto TEXT, \
            Stagione TEXT, \
            NumeroMaglia INT, \
            Ruolo TEXT, \
            Voto REAL, \
            FantaVoto REAL, \
            Gol INT, \
            Assist INT, \
            Rigori INT, \
            RigoriSbagliati INT, \
            Autogol INT, \
            Ammonizioni INT, \
            Espulsioni INT \
            )")

    def storeGiocatoriIntoDB(self, item):
        self.cur.execute("INSERT INTO Giocatori(\
            Squadra, \
            Giornata, \
            Nome, \
            NomeCompleto, \
            Stagione, \
            NumeroMaglia, \
            Ruolo, \
            Voto, \
            FantaVoto, \
            Gol, \
            Assist, \
            Rigori, \
            RigoriSbagliati, \
            Autogol, \
            Ammonizioni, \
            Espulsioni \
            )\
        VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )", \
        ( \
            item.get('Squadra', ''),
            item.get('Giornata', 0),
            item.get('Nome', ''),
            item.get('NomeCompleto', ''),
            item.get('Stagione', ''),
            item.get('NumeroMaglia', 0),
            item.get('Ruolo', ''),
            item.get('Voto', 0),
            item.get('FantaVoto', 0),
            item.get('Gol', 0),
            item.get('Assist', 0),
            item.get('Rigori', 0),
            item.get('RigoriSbagliati', 0),
            item.get('Autogol', 0),
            item.get('Ammonizioni', 0),
            item.get('Espulsioni', 0)
        ))
        self.con.commit()

    def process_item(self, item, gazzetta):
        self.storeGiocatoriIntoDB(item)
        return item

    def __del__(self):
        self.closeDB()

    def closeDB(self):
        self.con.close()


class FifaindexPipeline(object):

    def __init__(self):
        self.setupDBConn()
        self.createTables()

    def setupDBConn(self):
        self.con = lite.connect('test.db')
        self.cur = self.con.cursor()

    # I'm currently droping the tables if they exist before I run the script each time, so that
    # I don't get duplicate info.
    def createTables(self):
        self.dropPlayersTable()
        self.createPlayersTable()

    def dropPlayersTable(self):
        self.cur.execute("DROP TABLE IF EXISTS Players")

    def createPlayersTable(self):
        self.cur.execute("CREATE TABLE IF NOT EXISTS Players(id INTEGER PRIMARY KEY NOT NULL, \
            PlayerName TEXT, \
            Rating INT, \
            PotentialRating INT, \
            Height INT, \
            Weight INT, \
            PreferredFoot TEXT, \
            BirthDate TEXT, \
            Age INT, \
            PreferredPositions TEXT, \
            PlayerWorkRate TEXT, \
            BallControl INT, \
            Dribbling INT, \
            Marking INT, \
            SlideTackle INT, \
            StandTackle INT, \
            Aggression INT, \
            Reactions INT, \
            AttPosition INT, \
            Interceptions INT, \
            Vision INT, \
            Composure INT, \
            Crossing INT, \
            ShortPass INT, \
            LongPass INT, \
            Acceleration INT, \
            Stamina INT, \
            Strength INT, \
            Balance INT, \
            SprintSpeed INT, \
            Agility INT, \
            Jumping INT, \
            Heading INT, \
            ShotPower INT, \
            Finishing INT, \
            LongShots INT, \
            Curve INT, \
            FKAcc INT, \
            Penalties INT, \
            Volleys INT, \
            GKPositioning INT, \
            GKDiving INT, \
            GKHandling INT, \
            GKKicking INT, \
            GKReflexes INT, \
            PlayerId INT, \
            SeasonId INT, \
            PlayerPageLink TEXT, \
            Anticipation INT, \
            Creativity INT, \
            Nationality TEXT, \
            NationalTeam TEXT, \
            KitNumberNationalTeam INT, \
            ClubTeam TEXT, \
            KitNumberClubTeam INT \
            )")

    def storePlayersIntoDB(self, item):
        self.cur.execute("INSERT INTO Players(\
            PlayerName, \
            Rating, \
            PotentialRating, \
            Height, \
            Weight, \
            PreferredFoot, \
            BirthDate, \
            Age, \
            PreferredPositions, \
            PlayerWorkRate, \
            BallControl, \
            Dribbling, \
            Marking, \
            SlideTackle, \
            StandTackle, \
            Aggression, \
            Reactions, \
            AttPosition, \
            Interceptions, \
            Vision, \
            Composure, \
            Crossing, \
            ShortPass, \
            LongPass, \
            Acceleration, \
            Stamina, \
            Strength, \
            Balance, \
            SprintSpeed, \
            Agility, \
            Jumping, \
            Heading, \
            ShotPower, \
            Finishing, \
            LongShots, \
            Curve, \
            FKAcc, \
            Penalties, \
            Volleys, \
            GKPositioning, \
            GKDiving, \
            GKHandling, \
            GKKicking, \
            GKReflexes, \
            PlayerId, \
            SeasonId, \
            PlayerPageLink, \
            Anticipation, \
            Creativity, \
            Nationality, \
            NationalTeam, \
            KitNumberNationalTeam, \
            ClubTeam, \
            KitNumberClubTeam \
            )\
        VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )", \
        ( \
            item.get('PlayerName', ''),
            item.get('Rating', 0),
            item.get('PotentialRating', 0),
            item.get('Height', 0),
            item.get('Weight', 0),
            item.get('PreferredFoot', ''),
            item.get('BirthDate', ''),
            item.get('Age', 0),
            item.get('PreferredPositions', ''),
            item.get('PlayerWorkRate', ''),
            item.get('BallControl', 0),
            item.get('Dribbling', 0),
            item.get('Marking', 0),
            item.get('SlideTackle', 0),
            item.get('StandTackle', 0),
            item.get('Aggression', 0),
            item.get('Reactions', 0),
            item.get('AttPosition', 0),
            item.get('Interceptions', 0),
            item.get('Vision', 0),
            item.get('Composure', 0),
            item.get('Crossing', 0),
            item.get('ShortPass', 0),
            item.get('LongPass', 0),
            item.get('Acceleration', 0),
            item.get('Stamina', 0),
            item.get('Strength', 0),
            item.get('Balance', 0),
            item.get('SprintSpeed', 0),
            item.get('Agility', 0),
            item.get('Jumping', 0),
            item.get('Heading', 0),
            item.get('ShotPower', 0),
            item.get('Finishing', 0),
            item.get('LongShots', 0),
            item.get('Curve', 0),
            item.get('FKAcc', 0),
            item.get('Penalties', 0),
            item.get('Volleys', 0),
            item.get('GKPositioning', 0),
            item.get('GKDiving', 0),
            item.get('GKHandling', 0),
            item.get('GKKicking', 0),
            item.get('GKReflexes', 0),
            item.get('PlayerId', 0),
            item.get('SeasonId', 0),
            item.get('PlayerPageLink', ''),
            item.get('Anticipation', 0),
            item.get('Creativity', 0),
            item.get('Nationality', ''),
            item.get('NationalTeam', ''),
            item.get('KitNumberNationalTeam', 0),
            item.get('ClubTeam', ''),
            item.get('KitNumberClubTeam', 0)
        ))
        self.con.commit()


    def process_item(self, item, fifaindex):
        item.setdefault('PlayerWorkRate','NULL')
        self.storePlayersIntoDB(item)
        return item

    def __del__(self):
        self.closeDB()

    def closeDB(self):
        self.con.close()

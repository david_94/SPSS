#!/bin/bash
# Add git pull on cron
# Set hourly, bidaily, daily, weekly in the variable 'pulltime'

pulltime=hourly

# Delete config if exists
if [ ! -f '/etc/cron.d/gitpull' ]
then
  echo "Config file does not exist. Skipping..."
else
  rm '/etc/cron.d/gitpull'
fi

case "$pulltime" in
hourly) echo "0 * * * * root /opt/SPSS/dev-workspace/scripts/git-pull-script.sh" > /etc/cron.d/gitpull
;;
bidaily) echo "0 */12 * * * root /opt/SPSS/dev-workspace/scripts/git-pull-script.sh" > /etc/cron.d/gitpull
;;
daily) echo "0 0 * * * root /opt/SPSS/dev-workspace/scripts/git-pull-script.sh" > /etc/cron.d/gitpull
;;
weekly) echo "0 0 * * 0 root /opt/SPSS/dev-workspace/scripts/git-pull-script.sh" > /etc/cron.d/gitpull
;;
esac

service cron restart

#!/bin/bash
# Shell script for installing latest version of docker and docker-compose on Ubuntu 16.04.
#

# Exit immediately if a command exits with a non-zero status
set -e

# Verify that script execution has root privileges
if [[ $EUID -ne 0 ]]; then
	echo "Please run this script as root."
	exit 1
fi

# Check if ansible is installed
if ! command -v docker >/dev/null; then
        echo "Docker not found, next step will install ansible dependencies and git"
        sudo apt-get update -qq
        apt-get remove docker docker-engine docker.io
        apt-get install -y -qq git apt-transport-https ca-certificates curl software-properties-common composer
	composer require "twig/twig:^2.0"
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
        apt-get update
        apt-get install -y -qq docker-ce
        curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
        chmod +x /usr/local/bin/docker-compose
	exit 0
fi

# Print on screen current docker version
echo "Versioni attualmente installate:"
echo "- $(docker --version | cut -f 1 -d,)"
echo "- $(docker-compose --version | cut -f 1 -d,)"
exit 0

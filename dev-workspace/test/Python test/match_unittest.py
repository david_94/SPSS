import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import xml.etree.ElementTree as ET
import time

class TestRicercaVisualizzaTeam(unittest.TestCase):    
    def test_team1_0chars(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box1 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team1")))
            box2 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team2")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "submatch")))
            box1.send_keys("")
            box2.send_keys("unused_") #stringa "don't care"
            button.click()
            self.assertEqual('Social Perception and Sentiment in Soccer', 
                             driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
    def test_team1_1char(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box1 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team1")))
            box2 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team2")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "submatch")))
            box1.send_keys("d")
            box2.send_keys("unused_") #qui può andare qualsiasi valore, viene inserita una stringa che ha significato "don't care"
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
    def test_team1_numbers(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box1 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team1")))
            box2 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team2")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "submatch")))
            box1.send_keys("54876452")
            box2.send_keys("unused_") #qui può andare qualsiasi valore, viene inserita una stringa che ha significato "don't care"
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")   
    
    def test_team1_symbols(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box1 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team1")))
            box2 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team2")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "submatch")))
            box1.send_keys("!£$%&*")
            box2.send_keys("unused_") #qui può andare qualsiasi valore, viene inserita una stringa che ha significato "don't care"
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
    def test_team1_mix(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box1 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team1")))
            box2 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team2")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "submatch")))
            box1.send_keys("j45f6034$")
            box2.send_keys("unused_") #qui può andare qualsiasi valore, viene inserita una stringa che ha significato "don't care"
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
    def test_team1_notInDB(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box1 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team1")))
            box2 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team2")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "submatch")))
            box1.send_keys("Tennisecalcio")
            box2.send_keys("unused_") #qui può andare qualsiasi valore, viene inserita una stringa che ha significato "don't care"
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")   
    
    def test_team2_0chars(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box1 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team1")))
            box2 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team2")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "submatch")))
            box1.send_keys("unused_") #qui può andare qualsiasi valore, viene inserita una stringa che ha significato "don't care"
            box2.send_keys("")
            button.click()
            self.assertEqual('Social Perception and Sentiment in Soccer', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
    def test_team2_1char(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box1 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team1")))
            box2 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team2")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "submatch")))
            box1.send_keys("unused_") #qui può andare qualsiasi valore, viene inserita una stringa che ha significato "don't care"
            box2.send_keys("c")
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
    def test_team2_numbers(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box1 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team1")))
            box2 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team2")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "submatch")))
            box1.send_keys("unused_") #qui può andare qualsiasi valore, viene inserita una stringa che ha significato "don't care"
            box2.send_keys("87979790")
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")   
    
    def test_team2_symbols(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box1 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team1")))
            box2 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team2")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "submatch")))
            box1.send_keys("unused_") #qui può andare qualsiasi valore, viene inserita una stringa che ha significato "don't care"
            box2.send_keys("/**°_")
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
    def test_team2_mix(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box1 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team1")))
            box2 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team2")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "submatch")))
            box1.send_keys("unused_") #qui può andare qualsiasi valore, viene inserita una stringa che ha significato "don't care"
            box2.send_keys("_.34fa")
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
    def test_team2_notInDB(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box1 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team1")))
            box2 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team2")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "submatch")))
            box1.send_keys("unused_") #qui può andare qualsiasi valore, viene inserita una stringa che ha significato "don't care"
            box2.send_keys("GiglioDiCampagna")
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
    def test_equality(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box1 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team1")))
            box2 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team2")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "submatch")))
            box1.send_keys("Inter")
            box2.send_keys("Inter")
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
    def test_working(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box1 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team1")))
            box2 = driver.wait.until(EC.presence_of_element_located((By.NAME, "team2")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "submatch")))
            box1.send_keys("Milan")
            box2.send_keys("Bologna")
            button.click()
            self.assertEqual('Match found!', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
            
            
            
            
#init webdriver            
tree = ET.parse('configDriver.xml')
root = tree.getroot()
browser = root.find('.//browser[@name="{}"]'.format("browsername")).text
if browser == "Chrome":
    driver = webdriver.Chrome()
elif browser == "Firefox":
    driver = webdriver.Firefox()
else:
    driver.quit()  
driver.wait = WebDriverWait(driver, 5)

#launch tests and quit
suite = unittest.TestLoader().loadTestsFromTestCase(TestRicercaVisualizzaTeam)
unittest.TextTestRunner().run(suite)
time.sleep(4)
driver.quit()
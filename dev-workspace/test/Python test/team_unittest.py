import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import xml.etree.ElementTree as ET

class TestRicercaVisualizzaTeam(unittest.TestCase):    
    def test_0chars(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box = driver.wait.until(EC.presence_of_element_located((By.NAME, "team")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "subteam")))
            box.send_keys("")
            button.click()
            self.assertEqual('Social Perception and Sentiment in Soccer', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
    def test_1char(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box = driver.wait.until(EC.presence_of_element_located((By.NAME, "team")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "subteam")))
            box.send_keys("d")
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
    def test_manycharNUMBERS(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box = driver.wait.until(EC.presence_of_element_located((By.NAME, "team")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "subteam")))
            box.send_keys("3266064")
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")    
            
    def test_manycharSYMBOLS(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box = driver.wait.until(EC.presence_of_element_located((By.NAME, "team")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "subteam")))
            box.send_keys("%£$/*")
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
    def test_manycharMIX(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box = driver.wait.until(EC.presence_of_element_located((By.NAME, "team")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "subteam")))
            box.send_keys("834Hsdj$+-*/")
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
    def test_manycharLETTERSondb(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box = driver.wait.until(EC.presence_of_element_located((By.NAME, "team")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "subteam")))
            box.send_keys("Fiorentina")
            button.click()
            self.assertEqual('Team found!', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
    def test_manycharLETTERSnotondb(self):
        try:
            driver.get("http://90.147.89.104/index.php")
            box = driver.wait.until(EC.presence_of_element_located((By.NAME, "team")))
            button = driver.wait.until(EC.element_to_be_clickable((By.NAME, "subteam")))
            box.send_keys("Ascoltarelamusica")
            button.click()
            self.assertEqual('404: Page not found', driver.find_elements_by_xpath('//h1[@id="result"]')[0].text)
        except TimeoutException:
            print("Box or Button not found in google.com")
            
            
            
            
            
            
#init webdriver            
tree = ET.parse('configDriver.xml')
root = tree.getroot()
browser = root.find('.//browser[@name="{}"]'.format("browsername")).text
if browser == "Chrome":
    driver = webdriver.Chrome()
elif browser == "Firefox":
    driver = webdriver.Firefox()
else:
    driver.quit()  
driver.wait = WebDriverWait(driver, 5)

#launch tests and quit
suite = unittest.TestLoader().loadTestsFromTestCase(TestRicercaVisualizzaTeam)
unittest.TextTestRunner().run(suite)
driver.quit()
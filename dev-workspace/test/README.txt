CATEGORY PARTITION
_______________________________________________________________________________
1] 	Nella cartella categorypartition-tool\Binaries\input 
	sono presenti due tipologie di file
		- nessuna estensione --> categorie generate manualmente per il test dell'input
		- estensione tsl --> casi di test generati dal tool a partire dalle categorie
2] Steps per la generazione del file tsl
		- aprire una shell con diritti di amministratore
		- posizionarsi nella cartella categorypartition-tool\Binaries
		- lanciare l'eseguibile passando come parametro il file spss contenuto 
			nella cartella input
		- nella cartella input verrà generato un file .tsl contenente i casi di 
			test (se tutto è andato a buon fine sarà visualizzato un messaggio 
			di successo "6 test frames generated and written to \path\to\file.tsl")
			
.............................................
ESEMPIO FLOW (CMD)
> cd C:\...\categorypartition-tool\Binaries
> TSLgenerator-win32.exe input\spss
.............................................







PYTHON SCRIPT + SELENIUM
______________________________________________________________________________
1] 	Aggiungere il path della cartella Driver nella sezione Path delle variabili 
	d'ambiente del sistema
2]	Installare selenium per python (tramite pip) utilizzando il .whl nella
	cartella Librerie 
3]	Aprire il file configDriver.xml e inserire il nome del browser che si
	desidera utilizzare per il testing. Attualmente i valori possibili sono
	"Chrome" e "Firefox". Nel caso in cui venisse inserito un nome errato, lo
	script termina immediatamente.
3] 	Doppio click su player_unittest.py, team_unittest.py o match_unittest.py





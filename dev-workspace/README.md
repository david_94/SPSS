# SPSS - Development Workspace
>Ambiente di Sviluppo e Deploy per il progetto SPSS

## Operating System Compatibility

* Ubuntu 16.04

## Installation 

```sh
./prepare-docker-compose.sh
docker-compose up -d
```

After the installation, your server will be up
You can check with 

```sh
curl http://127.0.0.1
```

This setup will deploy:
- Apache2 webserver
- MySQL database
- Redis session handler
- phpMyAdmin service

## Usage

### phpMyAdmin access

phpMyAdmin runs on port 8080.

http://YOURIPADDRESS:8080/
username: root
password: PassWord_Difficile!! 

### In order to stop service, execute this command:

```sh
docker-compose down
```

### Delete all container and images

```sh
./delete-all.sh
```

## Release History

* 0.1.1 [2018-06-20]
 * Fix for management
* 0.1.0 [2018-05-01]
 * Completed basic deployment scripts
* 0.0.1 [2018-04-17]
 * Added script prepare dependecies

## Meta
| Autore          | Matricola |
|-----------------|-----------|
| Davide Merletti | 781122    |
| Giacomo Imola   | 783431    |
| Nicolas Perillo | 780921    |
| Paolo Velati    | 781621    |
